# vertx-shutdown

Launch simple verticle:
```
mvn clean package -Dverticle.class=SimpleVerticle && docker build -t shutdown . && docker run -p 8999:8999 shutdown
```