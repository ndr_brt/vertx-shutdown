package ndr.brt.vertx.shutdown;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.Router;

import java.util.concurrent.TimeUnit;

public class NoGraceVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) {
    int port = 8999;

    Router router = Router.router(vertx);

    GetAsyncOperationHandler.wire(vertx, router);

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(port)
      .onSuccess(result -> {
        System.out.println("Server listening on port " + port);
        startPromise.complete();
      })
      .onFailure(startPromise::fail);
  }
}
