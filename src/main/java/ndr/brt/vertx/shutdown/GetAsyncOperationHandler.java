package ndr.brt.vertx.shutdown;

import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class GetAsyncOperationHandler implements Handler<RoutingContext> {
  private final Vertx vertx;

  public static void wire(Vertx vertx, Router router) {
    router.get("/api/operation").handler(new GetAsyncOperationHandler(vertx));
  }

  public GetAsyncOperationHandler(Vertx vertx) {
    this.vertx = vertx;
  }

  @Override
  public void handle(RoutingContext context) {
    System.out.println("Async operation start");
    Promise<Void> promise = Promise.promise();
    vertx.setTimer(5000, time -> {
      System.out.println("Async operation done");
      promise.complete();
    });

    promise.future()
      .onSuccess(result -> context.end())
      .onFailure(context::fail);
  }
}
